import random
import numpy as np
from numpy.random import choice

choices_global = ['Rock', 'Paper', 'Scissors']

initialWinRate = 0
initialScore = 0
initialRockProb = 1/3
initialPaperProb = 1/3
initialScissorsProb = 1/3

winningMatrix = {'RockRock': 0, 'RockPaper': -1, 'RockScissors': 1, 'PaperRock': 1, 'PaperPaper': 0,
                 'PaperScissors': -1, 'ScissorsRock': -1, 'ScissorsPaper': 1, 'ScissorsScissors': 0}


def generate_random_percentages():

    percentages = [None]* 3
    x = 1
    y = 1
    z = 1
    while x + y + z != 1.0:
        x = round(random.uniform(0.0, 1.0), 2)
        y = round(random.uniform(0.0, 1.0), 2)
        z = round(random.uniform(0.0, 1.0), 2)
    percentages[0] = x
    percentages[1] = y
    percentages[2] = z

    return percentages


def reset_score (population):
    for i in range(len(population)):
        population[i].score = 0;
    return population


def winner(draw1,draw2):
    z_winner = draw1+draw2
    if (winningMatrix[z_winner] == 0):
        result = 'draw'
    elif (winningMatrix[z_winner] == 1):
        result = 'win'
    elif (winningMatrix[z_winner] == -1):
        result = 'lose'
    return  result


def choice_against_enemy(index):
    normal_choices = ['Rock', 'Paper', 'Scissors']
    new_choices = [None] * len(normal_choices)
    new_choices[0] = normal_choices[index]
    if new_choices[0] == 'Rock':
        new_choices[1] = 'Paper'
        new_choices[2] = 'Scissors'
    elif new_choices[0] == 'Paper':
        new_choices[1] = 'Scissors'
        new_choices[2] = 'Rock'
    elif new_choices[0] == 'Scissors':
        new_choices[1] = 'Rock'
        new_choices[2] = 'Paper'
    return new_choices


def calculate_score (result, draw1, draw2):
    score = 0
    if result == 'draw':
        score = score + 0
    if draw2 == 'Rock' and draw1 == 'Paper':
        score = score + winningMatrix['RockPaper']
    if draw2 == 'Rock' and draw1 == 'Scissors':
        score = score + winningMatrix['RockScissors']
    if draw2 == 'Paper' and draw1 == 'Rock':
        score = score + winningMatrix['PaperRock']
    if draw2 == 'Paper' and draw1 == 'Scissors':
        score = score + winningMatrix['PaperScissors']
    if draw2 == 'Scissors' and draw1 == 'Rock':
        score = score + winningMatrix['ScissorsRock']
    if draw2 == 'Scissors' and draw1 == 'Paper':
        score = score + winningMatrix['ScissorsPaper']
    return score


class GeneralPlayer:

    percentage = [None] * 3

    def __init__(self, score, choices_for_player, percentage_for_player):
        self.score = score
        self.choices = choices_for_player
        self.percentage = percentage_for_player

    def draw_once(self):
        draw = choice(self.choices, 1, p=self.percentage)
        draw_index = self.choices.index(draw)
        return draw, draw_index

    def draw_against_enemy(self,new_choices):
        draw = choice(new_choices, 1, p=self.percentage)
        draw_index = new_choices.index(draw)
        return draw, draw_index


def starting_pop(numar_indivizi):
    population = [None] * numar_indivizi
    for i in range(numar_indivizi):
        percentages = generate_random_percentages()
        individ = Chromosome(0, choices_global, percentages)
        population[i] = individ
    return population


def worst_fitness(population):

    fitness = [None] * len(population)

    for i in range(len(population)):
        fitness[i] = population[i].score
    index = fitness.index(min(fitness))
    return population[index]


def select_individ(population, k):

    aspirants = [None] * k
    fitness = [None] * k
    for i in range(k):
        aspirants[i] = random.choice(population)
        fitness[i] = aspirants[i].score
    index = fitness.index(max(fitness))
    return aspirants[index]


def breed_by_crossover(parent1, parent2):

    child = Chromosome(0, choices_global, [0, 0, 0])
    child.percentage[0] = (parent1.percentage[0]+parent2.percentage[0])/2
    child.percentage[1] = (parent1.percentage[1]+parent2.percentage[1])/2
    child.percentage[2] = (parent1.percentage[2]+parent2.percentage[2])/2

    return child


def random_mutate (population, mutation_prob):
    for individual in range(len(population)):
        x = random.uniform(0,0.1)
        if x < mutation_prob :
            p0 = population[individual].percentage[0]
            p1 = population[individual].percentage[1]
            p2 = population[individual].percentage[2]
            population[individual].percentage[0] = (population[individual].percentage[0] + population[individual].percentage[1])/2
            population[individual].percentage[1] = population[individual].percentage[1] / 2
            population[individual].percentage[2] = population[individual].percentage[2] + 0.125

            sum = population[individual].percentage[0]+population[individual].percentage[1]+population[individual].percentage[2]

            dif = 1-sum

            if dif > 0:
                population[individual].percentage[0] = population[individual].percentage[0] + dif
            if dif < 0:
                population[individual].percentage[0] = population[individual].percentage[0] + dif
                if population[individual].percentage[0] < 0:
                    population[individual].percentage[0] = population[individual].percentage[0] - dif
                    population[individual].percentage[1] = population[individual].percentage[1] + dif
                    if population[individual].percentage[1] < 0:
                        population[individual].percentage[1] = population[individual].percentage[1] - dif
                        population[individual].percentage[2] = population[individual].percentage[2] + dif

    return population


class OnlyRockPlayer:

    choices = ['Rock', 'Paper', 'Scissors']
    percentage = [1.0, 0.0, 0.0]


class EqualPlayer:

    choices = ['Rock', 'Paper', 'Scissors']
    percentage = [1/3, 1/3, 1/3]


class NeverPaper:

    choices = ['Rock', 'Paper', 'Scissors']
    percentage = [0.5, 0, 0.5]


class Game:

    # choices = ['Rock', 'Paper', 'Scissors']
    winningMatrix = {'RockRock': 0, 'RockPaper': -1, 'RockScissors': 1, 'PaperRock': 1, 'PaperPaper': 0, 'PaperScissors': -1, 'ScissorsRock': -1, 'ScissorsPaper': 1, 'ScissorsScissors': 0}

    def __init__(self, score, numberOfRounds, rockProb, paperProb, scissorsProb):
        self.score = score
        self.n = numberOfRounds
        self.rockProb = rockProb
        self.paperProb = paperProb
        self.scissorsProb = scissorsProb

    percentage = []

    def play_a_round(self, player1, player2):

        draw_p1, index_p1 = player1.draw_once()
        # player2_choices = choice_against_enemy(last_round_index)
        # last_round_index = index_p1
        # print ('player 2 choices by player 1 draw: ', player2_choices)
        draw_p2, index_p2 = player2.draw_once()
        print ('index_p1: ', index_p1, 'index_p2', index_p2)
        print('drawOne: ', draw_p1, 'drawTwo', draw_p2)
        result = winner(draw_p1[0], draw_p2[0])
        print ('procentaje player 2: ', player2.percentage)

        if result == 'draw':
            print('Player 1 and Player 2 are EQUAL')
        elif result == 'win':
            print('PLayer 1 win, PLayer 2 LOSE')
        elif result == 'lose':
            print('Player 1 lose, Player 2 WIN')

        player2.score += calculate_score(result,draw_p1,draw_p2)
        self.score += calculate_score(result, draw_p1[0], draw_p2[0])
        # return last_round_index

    def first_round_game(self,player1, player2):

        draw_p1, index_p1 = player1.draw_once()
        first_round_index = index_p1
        draw_p2, index_p2 = player2.draw_once()
        print('index_p1: ', index_p1, 'index_p2', index_p2)
        print('drawOne: ', draw_p1, 'drawTwo', draw_p2)
        result = winner(draw_p1[0], draw_p2[0])
        if result == 'draw':
            print('Player 1 and Player 2 are EQUAL')
        elif result == 'win':
            print('PLayer 1 win, PLayer 2 LOSE')
        elif result == 'lose':
            print('Player 1 lose, Player 2 WIN')
        player2.score += calculate_score(result,draw_p1,draw_p2)

        self.score += calculate_score(result, draw_p1[0], draw_p2[0])
        print('score is: ', self.score)
        return first_round_index

    def play_n_rounds(self, player1, player2):
        first_round = 0
        print('Se vor juca ', self.n, ' runde')
        for i in range(self.n):
            if first_round == 0:
                print('Runda ', i+1)
                last_round_index = self.first_round_game(player1, player2)
                print('last round index in first round ', last_round_index)
                first_round += 1
            elif first_round != 0:
                print('Runda ', i + 1)
                # print('last round index in ', i ,'round  ', last_round_index)
                # last_round_index = self.play_a_round(player1, player2, last_round_index)
                self.play_a_round(player1,player2)
                print('Sfarsitul rundei ', i + 1)


class Chromosome(GeneralPlayer):

    def fitness_score(self):
        return self.score


print('Numarul de indivizi din populatie ')
pop_size = int(input())

print('Numarul de generatii ')
numar_generatii = int(input())

population = starting_pop(pop_size)

print('Pentru selectia "parintilor" specificati numarul de aspiranti')
number_of_aspirants = int(input())

print('Introduceti numarul de runde pe care fiecare individ sa le joace contra jucatorului stabilit')

n = int(input())

for generation in range(numar_generatii):

    idealScore = n * max(winningMatrix.values())
    choices = ['Rock', 'Paper', 'Scissors']
    random_percentages = generate_random_percentages()
    only_rock = [1.0, 0.0, 0.0]
    only_paper = [0.0, 1.0, 0.0]
    equal_player = [1/3, 1/3, 1/3]
    game = Game(initialScore, n, initialRockProb, initialPaperProb, initialScissorsProb)

    new_player1 = GeneralPlayer(initialScore, choices, only_rock)
    for individ in range(len(population)):
        print('Joaca individul ', individ, 'cu caracteristicile ', population[individ].score, population[individ].choices, population[individ].percentage)
        game.play_n_rounds(new_player1, population[individ])
        print('Scorul individului cu numarul ', individ, 'este ', population[individ].score, ' iar scorul maxim este ', idealScore)

    parent_1 = select_individ(population, number_of_aspirants)
    parent_2 = select_individ(population, number_of_aspirants)

    child = breed_by_crossover(parent_1, parent_2)

    the_worst_one = worst_fitness(population)

    population.remove(the_worst_one)
    population.append(child)

    mutation_rate = 0.01

    population = random_mutate(population, mutation_rate)

    population = reset_score(population)

    print('probabilitati random ', random_percentages)




